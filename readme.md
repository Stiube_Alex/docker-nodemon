# Docker Nodemon

This is a small experimental project demonstrating containerization using [Docker](https://www.docker.com/). The project consists of a basic [Node.js](https://nodejs.org/en) server.

---

## Running the App

The project has 2 `.sh` scripts that handle the running and cleaning the project.

1. `compose.sh` used to build and run the project

```sh
#!/bin/bash

docker-compose up --build
```

`#!/bin/bash:` is called a shebang, and it tells the system that this script should be interpreted using the Bash shell. It specifies the path to the Bash interpreter.

`docker-compose up --build:`  the main command being executed in the script. It is using the docker-compose tool to manage Docker containers:

- `docker-compose `is a command-line tool used to define and manage multi-container Docker applications using a YAML file (usually named `docker-compose.yaml`)

- `up` is a subcommand of `docker-compose` that is used to start and run the containers defined in the `docker-compose.ymla` file.

- `--build` is an optional flag that tells `docker-compose` to rebuild the container images before starting the containers. It ensures that any changes made to the Dockerfile or build context are incorporated.


2. `clear.sh` used to remove the files from docker

```sh
#!/bin/bash

docker-compose down --volumes --rmi all && docker system prune --all -f
```
`docker-compose down --volumes --rmi all`:  is using the `docker-compose` tool to stop and remove containers, networks, and volumes defined in the `docker-compose.yaml` file. The specific options used in this command are:

- `down:` is a subcommand of `docker-compose` used to stop and remove containers, networks, and volumes

- `--volumes`: option used to remove the volumes associated with the containers defined

- `--rmi all`: option used to remove all the container images

`&&`: is a separator used to run multiple commands sequentially. It ensures that the next command is executed only if the previous one succeeds.

`docker system prune --all -f`: This command is using the docker command-line tool to remove unused Docker resources. The specific options used in this command are:

- `system prune`: This command is used to remove unused data such as stopped containers, dangling images, and unused networks

- `--all`: This option is used to remove all unused data, not just the ones created by `docker-compose`

- `-f`: This option is used to force the removal without asking for confirmation.


### ⚠️ Warning

> **Important:** `docker system prune --all -f` will delete every unused docker resource (containers, images,  volumes, etc.). It also doens't require any additional prompts.

---

## Dockerfile

A `Dockerfile` is a text file that contains a set of instructions used to build a Docker image. It specifies the base image to use, the files and directories to include in the image, the commands to run during the image build process, and the settings and configurations for the container when it is instantiated. The `Dockerfile` serves as a blueprint for creating reproducible and self-contained Docker images that can be used to deploy and run applications in a containerized environment.

```docker
FROM node:alpine
```

Sets the base image for the Docker image. In this case, it uses the `node` image with the `alpine` variant. The `alpine` variant is a lightweight version of the image based on the Alpine Linux distribution.

```docker
WORKDIR /usr/src/node
```

Sets the working directory inside the container where subsequent commands will be executed. In this case, it sets the working directory to `/usr/src/node`. This directory will be created if it doesn't exist.

```docker
COPY ./src .
```

Copies the contents of the `./src` directory from the host machine to the current working directory in the container. The dot (`.`) represents the current working directory. This step allows the Node.js application files to be added to the container.

```docker
RUN npm install
```

Executes the `npm install` command during the build process of the Docker image. It installs the dependencies specified in the `package.json` file of the Node.js application. The `RUN` instruction is used for executing commands during the build process.

```docker
CMD ["npm", "run", "start"]
```

Specifies the command to be executed when the container starts. It uses the JSON array format to define the command as an array of strings. In this case, it runs the `npm run start` command, which starts the Node.js application using the script defined in the `package.json` file.

---

## Docker Compose

The `docker-compose.yaml` file is used to define and configure multi-container applications in Docker. It allows you to specify the services, networks, volumes, and other configurations needed for your application to run. It simplifies the process of running and managing multiple containers as a single unit, making it easier to deploy and scale complex applications.

```yaml
version: "3"
```

Specifies the version of the Docker Compose file format being used. In this case, it's version 3.

```yaml
services:
node-app:
container_name: node_app
build: .
ports: - 8080:3000
volumes: - app:/usr/src/node
restart: always
```

`services`: Section where you define the services (containers) for your application.

`node-app`: Name of the service/container.

`container_name`: Custom name for the container.

`build`: Indicates where the Docker image for this service should be built using the current directory (`.` refers to the current directory).

`ports`: Maps the container's port to the host's port.

- `8080:3000`: Maps the host's port `8080` to the container's port `3000`. Requests to `localhost:8080` will be forwarded to the running container's port `3000`.

`volumes`: Allows you to mount volumes between the host and the container.

- `app:/usr/src/node`: Creates a named volume called `app` and mounts it to the container's `/usr/src/node` directory. Any changes made in the host's `./src` directory will be reflected in the container.

`restart`: Ensures that the container will be automatically restarted if it crashes or if the Docker daemon is restarted.

```yaml
volumes:
  app:
    driver: local
    driver_opts:
      device: ./src
      type: none
      o: bind
```

`volumes`: Section that defines named volumes used in the services.

`app`: Name of the volume.

`driver`: This specifies the volume driver as local, meaning the volume is managed by the local Docker engine.

`driver_opts`: Additional options for the volume driver.

- `device`: Specifies the directory on the host machine (`./src`) that will be mounted to the volume. Changes in this directory will be synchronized with the volume.

- `type`: Indicates the volume specific type (`none` for this case).

- `o`: Specifies that the volume is a `bind` mount in this case, which means the host's directory will be directly mounted into the container (short for 'options').

---

## Docker Ignore

The `.dockerignore` file helps to specify files, directories, and patterns that should be excluded from the Docker build context, resulting in a smaller and more efficient Docker image.

`src/node_modules`

Instructs Docker to ignore the `node_modules` directory under the `src` directory. The `node_modules` directory typically contains the installed dependencies for a Node.js project and can be quite large. Ignoring it in the Docker build process helps to improve build performance and reduces the size of the Docker image.

```
.dockerignore
docker-compose.*
Dockerfile
```

Specify additional files to be ignored by Docker during the build process:

- `.dockerignore`: The `.dockerignore` file itself is ignored to avoid including it in the Docker image.

- `docker-compose.\*`: Any file with the prefix `docker-compose`. is ignored. This ensures that Docker Compose configuration files (e.g., `docker-compose.yml`, `docker-compose.yaml`) are not included in the Docker image.

- `Dockerfile`: The `Dockerfile` itself is ignored to prevent it from being included in the Docker image. The Dockerfile is only used during the build process and is not required to be present in the final image.

```
.gitignore
```

Excludes the `.gitignore` file from being included in the Docker image. The `.gitignore` file is used to specify files and directories that should be ignored by Git, and it is not necessary for the Docker image.

```
*.sh
```

Ignores any files with the `.sh` extension. It is common to use the `.sh` extension for shell scripts, and in this case, any shell scripts present in the project directory will be excluded from the Docker image.

```
logo.*
```

Ignores any files with the `logo` prefix and any file extension. For example, it would ignore files like `logo.png`, `logo.jpg`, etc. This can be useful if you have specific logo files that are not required in the Docker image.

---
