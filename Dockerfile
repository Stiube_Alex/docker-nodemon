FROM node:alpine

WORKDIR /usr/src/node

COPY ./src .

RUN npm install

CMD ["npm", "run", "start"]